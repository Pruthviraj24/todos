let todoItemsContainer = document.getElementById("todoItemsContainer");
let saveTodoButton = document.getElementById("saveTodoButton");


// Getting Data from local storage 

function getTodoListFromLocalStorage() {
    let stringifiedTodoList = localStorage.getItem("todoList");
    let parsedTodoList = JSON.parse(stringifiedTodoList);
  if (parsedTodoList === null) {
        return [];
  } else {
        return parsedTodoList;
  }
}


let todoList = getTodoListFromLocalStorage();
let todosCount = todoList.length;

//Saving todoList to localStorage

saveTodoButton.onclick =  ()=> {
        localStorage.setItem("todoList", JSON.stringify(todoList));
};


// Add todo Item onClicking Enter

function onAddTodo(userInputValue) {

    todosCount = todosCount + 1;
    let newTodo = {
        text: userInputValue,
        uniqueNo: todosCount,
        isChecked: false,
    };
    todoList.push(newTodo);

    createAndAppendTodo(newTodo);

}


// Updateing UserInput

let userInputElement = document.getElementById("todoUserInput");

userInputElement.addEventListener("keydown", (event) => {
        console.log(event.target.key);
        const eventValue = event.target.value;
        const eventKey = event.key;
    if (eventKey === "Enter") {
        if (eventValue === "") {
            alert("Enter Valid Todo");
            return;
        } else {
            onAddTodo(event.target.value);
            event.target.value = "";
        }
    }
});



// On change of Todo status

function onTodoStatusChange(checkboxId, labelId, todoId) {
        let checkboxElement = document.getElementById(checkboxId);
        let labelElement = document.getElementById(labelId);
        labelElement.classList.toggle("checked");

        let todoIndex = todoList.findIndex((eachTodo) =>{
        return todoId === "todo" + eachTodo.uniqueNo;
  });
    if (todoList[todoIndex].isChecked === true) {
        todoList[todoIndex].isChecked = false;
    } else {
        todoList[todoIndex].isChecked = true;
    }
}




//Delete Todo Item Function 

function onDeleteTodo(todoId) {
  let todoElement = document.getElementById(todoId);
  todoItemsContainer.removeChild(todoElement);
  let deleteElementIndex = todoList.findIndex((eachTodo)=>{
    let eachTodoId = "todo" + eachTodo.uniqueNo;
    if (eachTodoId === todoId) {
      return true;
    } else {
      return false;
    }
  });

  todoList.splice(deleteElementIndex, 1);
  updateActiveTodoItem()
}


//Update active todo-Item onDeleting single todo-Item
function updateActiveTodoItem(){
    let activeTodoItems = document.getElementById("active-todo")
    const activeItems = todoList.filter(eachTodo=> eachTodo.isChecked === false)
    activeTodoItems.textContent = `${activeItems.length} Tasks left`
}




//Clearing all todoList Items
function deleteAllTodo(deleteAllTodo){
    for(let todo of deleteAllTodo){
        let todoItem = document.getElementById(`todo${todo.uniqueNo}`)
        if(todoItem !== null){
            todoItemsContainer.removeChild(todoItem)
        }
       
    }
    
}


//Creating and appending new-todo item

function createAndAppendTodo(todo) {
        let todoId = "todo" + todo.uniqueNo;
        let checkboxId = "checkbox" + todo.uniqueNo;
        let labelId = "label" + todo.uniqueNo;

        let todoElement = document.createElement("li");
        todoElement.classList.add("todo-item-container");
        todoElement.id = todoId;
        todoItemsContainer.appendChild(todoElement);
        let inputElement = document.createElement("input");
        inputElement.type = "checkbox";
        inputElement.id = checkboxId;
        inputElement.checked = todo.isChecked;

        // Changing todoItem status.
    inputElement.onclick =  ()=> {
        onTodoStatusChange(checkboxId, labelId, todoId);
        

        //Updating active todo items count
        updateActiveTodo()
    };

        inputElement.classList.add("checkbox-input");
        todoElement.appendChild(inputElement);

        let labelContainer = document.createElement("div");
        labelContainer.classList.add("label-container");
        todoElement.appendChild(labelContainer);

        let labelElement = document.createElement("label");
        labelElement.setAttribute("for", checkboxId);
        labelElement.id = labelId;
        labelElement.classList.add("checkbox-label");
        labelElement.textContent = todo.text;
    if (todo.isChecked === true) {
        labelElement.classList.add("checked");
        }
        labelContainer.appendChild(labelElement);

        let deleteIcon = document.createElement("i");
        deleteIcon.classList.add("fa-solid", "fa-xmark", "delete-icon");
        labelContainer.appendChild(deleteIcon);


    // Deleting todo-item
    deleteIcon.onclick =  ()=> {
        onDeleteTodo(todoId);
    };


    //clear all todo-items
    const deleteAll = document.getElementById("clearAll")

    deleteAll.onclick = ()=>{
        localStorage.clear()
        todosCount = 0
        deleteAllTodo(todoList)
        todoList = []
        updateActiveTodo()
    }

    function updateActiveTodo(){
        let activeTodoItems = document.getElementById("active-todo")
        const activeItems = todoList.filter(eachTodo=> eachTodo.isChecked === false)
        activeTodoItems.textContent = `${activeItems.length} Tasks left`
    }
    updateActiveTodo()
}





//Display all todo onClick All button
let displayAllTodoButton = document.getElementById("allTodo")



function displayAllTodo(){
    deleteAllTodo(todoList)
    for (let todo of todoList) {
        createAndAppendTodo(todo);
    }
}


displayAllTodoButton.onclick = ()=>{
    displayAllTodo()
}




//Display Only Active Todo
const displayActiveTodoButton = document.getElementById("activeTodo")

displayActiveTodoButton.onclick = ()=>{
    
    deleteAllTodo(todoList)
    let activeTodoList = todoList.filter(eachTodo=> eachTodo.isChecked === false)
    todosCount = activeTodoList.length
    for(let todo of activeTodoList){
        createAndAppendTodo(todo)
    }

}




// Display only Completed Todos
const displayCompletedTodoList = document.getElementById("completedTod")

displayCompletedTodoList.onclick = ()=>{
    let completedTodo = todoList.filter(eachTodo => eachTodo.isChecked === true)
    todosCount = completedTodo.length
    deleteAllTodo(todoList)
    for(let todo of completedTodo){
        createAndAppendTodo(todo)
    }

}



function main(){
    for (let todo of todoList) {
        createAndAppendTodo(todo);
    }
}

main()